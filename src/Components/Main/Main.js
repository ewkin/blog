import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import './Main.css';


const Main = props => {
    const keys = Object.keys(props.posts);
    return (
        <Grid item xs={12} md={8}>
            <Typography variant="h6" gutterBottom>
                {props.title}
            </Typography>
            <Divider />
            {keys.map((key) => (
                <article key={key} className="Post" onClick={()=>props.clicked(key)}>
                    <h1>{props.posts[key].title}</h1>
                    <div>{props.posts[key].time}</div>
                    <div className="Body">
                        <p className="Author">{props.posts[key].body.substring(0, 100)}...</p>
                    </div>
                </article>
            ))}
        </Grid>
    );
};

export default Main;
