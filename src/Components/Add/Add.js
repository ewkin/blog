import React, {useEffect, useState} from 'react';
import axiosBlog from "../../axios-blog";
import Header from "../Header/Header";
import {sections} from "../../constants";
import Container from "@material-ui/core/Container";
import './Add.css';

const Add = props => {
    const [posts, setPosts] = useState({
        title: '',
        body: '',
        time:'',
    });

    useEffect(()=>{
        if(props.match.params.action){
            const fetchData = async () =>{
                const response = await axiosBlog.get('/posts/'+props.match.params.action+'.json');
                setPosts(response.data);
            };
            fetchData().catch(console.error);
        }
    },[props.match.params.action])


    const postInput = event => {
        const {name, value} = event.target;
        let time = new Date().toLocaleString();
        setPosts(prevState => ({
            ...prevState,
            [name]: value,
            time: time,
        }));
    };

    const postHandler = async event =>{
        event.preventDefault();
        if(props.match.params.action){
            try {
                await axiosBlog.put('/posts/'+props.match.params.action+'.json', {...posts});
            } finally {
                props.history.push('/');
            }

        } else {
            try {
                await axiosBlog.post('/posts.json', {...posts});
            } finally {
                props.history.push('/');
            }
        }
    };

    const pathHandler = link =>{
        props.history.push({
            pathname: link,
        });
    };

    return (
        <Container maxWidth="lg">
            <Header title="Blog" sections={sections} path={pathHandler} />
            <form onSubmit={postHandler}>
                <div>
                    <input
                        required placeholder="Title"
                        type="text" name="title"
                        value={posts.title} onChange={postInput}
                    />
                </div>
                <div>
                    <textarea
                        required placeholder="New post"
                        type="text" name="body"
                        value={posts.body} onChange={postInput}
                    />
                </div>
                <button type="submit" className="Button">Send</button>
            </form>
        </Container>
    );
};

export default Add;