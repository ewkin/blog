import React from 'react';
import Container from "@material-ui/core/Container";
import Header from "../Header/Header";
import {sections, sidebar, useStyles} from "../../constants";
import Grid from "@material-ui/core/Grid";
import Sidebar from "../Sidebar/Sidebar";
import Subscribe from "../../Containers/Subscribe/Subscribe";

const About = props => {
    const classes = useStyles();
    const pathHandler = link =>{
        props.history.push({
            pathname: link,
        });
    };
    let page =(
        (<Subscribe/>)
    );

    return (
        <Container maxWidth="lg">
            <Header title="Blog" sections={sections} path={pathHandler} />
            <main>
                <Grid container spacing={4}>
                </Grid>
                <Grid container spacing={5} className={classes.mainGrid}>
                    <Grid item xs={12} md={8}>
                        {page}
                    </Grid>
                    <Sidebar
                        title={sidebar.title}
                        description={sidebar.description}
                        archives={sidebar.archives}
                        social={sidebar.social}
                        link = {pathHandler}
                    />
                </Grid>
            </main>
        </Container>
    );
};

export default About;