import React, {useEffect, useState} from 'react';
import './FullPost.css';
import axiosBlog from "../../axios-blog";
import Header from "../Header/Header";
import {sections} from "../../constants";
import Container from "@material-ui/core/Container";


const FullPost = props => {
    const [post, setPost] = useState({});

    useEffect(()=>{
        const fetchData = async () =>{
            const response = await axiosBlog.get('/posts/'+props.match.params.id+'.json');
            setPost(response.data);
        };

        fetchData().catch(console.error);
    },[props.match.params.id])


    const pathHandler = link =>{
        props.history.push({
            pathname: link,
        });
    };

    const deletePost =()=>{
        const fetchData = async () =>{
            const response = await axiosBlog.delete('/posts/'+props.match.params.id+'.json');
            setPost(response.data);
        };
        fetchData().catch(console.error);
        props.history.push('/');

    };

    const editPost = () => {
        props.history.push({
            pathname: '/add/'+props.match.params.id,
        });
    };

    return  (
        <Container maxWidth="lg">
        <Header title="Blog" sections={sections} path={pathHandler} />
        <article className="FullPost">
            <h1>{post.title}</h1>
            <p>{post.time}</p>
            <p>{post.body}</p>
            <button onClick={editPost} className="Button">Edit</button>
            <button onClick={deletePost} className="Button">Delete</button>
        </article>
        </Container>

    );
};

export default FullPost;