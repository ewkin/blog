import React, {useEffect, useState} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Header from '../../Components/Header/Header';
import Main from '../../Components/Main/Main';
import Sidebar from '../../Components/Sidebar/Sidebar';
import {sections, sidebar, useStyles} from "../../constants";
import axiosBlog from "../../axios-blog";


const Blog = props => {
    const classes = useStyles();
    const [posts, setPosts] = useState([]);


    useEffect(()=>{
        const fetchData = async () =>{
            const response = await axiosBlog.get('/posts.json');
            setPosts(response.data);

        }
        fetchData().catch(console.error);
    },[]);

    const fullPostHandler = id =>{
        props.history.push({
            pathname: '/posts/'+id,
        });
    };
    const pathHandler = link =>{
        props.history.push({
            pathname: link,
        });
    };

    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="lg">
                <Header title="Blog" sections={sections} path={pathHandler} />
                <main>
                    <Grid container spacing={4}>
                    </Grid>
                    <Grid container spacing={5} className={classes.mainGrid}>
                        <Main title="From the firehose" posts={posts} clicked ={fullPostHandler}
                        />
                        <Sidebar
                            title={sidebar.title}
                            description={sidebar.description}
                            archives={sidebar.archives}
                            social={sidebar.social}
                            link = {pathHandler}
                        />
                    </Grid>
                </main>
            </Container>
        </React.Fragment>
    );
}

export default Blog;
