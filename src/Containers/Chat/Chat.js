import React, {useEffect, useState} from 'react';
import moment from 'moment';
import InputText from "../../Components/InputText/InputText";
import Messages from "../../Components/Messages/Messages";

import './Chat.css';
import Container from "@material-ui/core/Container";
import Header from "../../Components/Header/Header";
import {sections} from "../../constants";



const Chat = props => {
    const [messages, setMessages] = useState([]);
    const [myMessage, setMyMessage] = useState('');

    const request = async url  => {
        let response = await fetch(url);
        if (response.ok) {
            return await response.json();
        } else {
            console.log("Ошибка HTTP: " + response.status);
        };
    };

    useEffect(()=>{
        const fetchData = async () => {
            setMessages(await request('http://146.185.154.90:8000/messages'));
        }
        fetchData();
    },[]);

    useEffect(()=>{
        setInterval(async () => {
            if(messages.length>1){
                let datetime = messages[messages.length-1].datetime;
                const response = await request('http://146.185.154.90:8000/messages?datetime='+datetime);
                if(response.length>0){
                    setMessages([...messages, response[0]]);
                }
            }
        }, 2000);

    },[messages]);

    const handleChange = (event)=> {
        setMyMessage(event.target.value);
    };

    const handleSubmit = (event) => {
        if(myMessage.length>0){
            const postMessage = async ()=>{
                const url = 'http://146.185.154.90:8000/messages';
                const data = new URLSearchParams();
                data.set('message', myMessage);
                data.set('author', 'Ignat');
                const response = await fetch(url, {
                    method: 'post',
                    body: data,
                });
                if (!response.ok) {
                    console.log("Ошибка HTTP: " + response.status);
                }
            };
            postMessage();
        };
        setMyMessage('');
        event.preventDefault();
    };
    const pathHandler = link =>{
        props.history.push({
            pathname: link,
        });
    };
    return (
        <Container maxWidth="lg">
            <Header title="Blog" sections={sections} path={pathHandler} />
            <div className="page-content page-container" id="page-content">
                <div className="padding">
                    <div className="row container d-flex justify-content-center">
                        <div className="col-md-6">
                            <div className="card card-bordered">
                                <div className="card-header">
                                    <h4 className="card-title"><strong>Chat</strong></h4>
                                </div>
                                <div className="ps-container">
                                    <div id = "chat">
                                        {messages.map(message =>(
                                          <Messages
                                              key = {message._id}
                                              author = {message.author}
                                              message={message.message}
                                              date = {moment(message.datetime).format('MMMM Do YYYY, h:mm:ss a')}
                                          />
                                        ))}
                                    </div>
                                </div>
                                <InputText
                                    value={myMessage}
                                    handleChange={handleChange}
                                    handleSubmit={handleSubmit}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    );
};

export default Chat;