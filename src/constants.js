import React from "react";
import ChatIcon from '@material-ui/icons/Chat';
import GitHubIcon from "@material-ui/icons/GitHub";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import {makeStyles} from "@material-ui/core/styles";

export const sections = [
    { title: 'Home', url: '/' },
    { title: 'Add', url: '/add' },
    { title: 'About', url: '/about' },
    { title: 'Contacts', url: '/contacts'},
    { title: <ChatIcon/>, url: '/chat' }
];

export const useStyles = makeStyles((theme) => ({
    mainGrid: {
        marginTop: theme.spacing(3),
    },
}));

export const sidebar = {
    title: 'About',
    description:
        'Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.',
    archives: [
        { title: 'January 2021'},

    ],
    social: [
        { name: 'GitHub', icon: GitHubIcon, link:'https://bitbucket.org/ewkin/workspace/projects/NODE'},
        { name: 'Twitter', icon: TwitterIcon, link:'https://twitter.com/navalny' },
        { name: 'Facebook', icon: FacebookIcon, link:'https://www.facebook.com/navalny/' },
    ],
};
