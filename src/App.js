import React from 'react';
import Blog from "./Containers/Blog/Blog";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import FullPost from "./Components/FullPost/FullPost";
import Subscribe from "./Containers/Subscribe/Subscribe";
import Chat from "./Containers/Chat/Chat";
import Add from "./Components/Add/Add";
import Footer from "./Components/Footer/Footer";
import About from "./Components/About/About";
import Contacts from "./Components/Contacts/Contacts"


const App = () => {

  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={Blog}/>
              <Route path="/add/:action" component={Add}/>
              <Route path="/add/" component={Add}/>
              <Route path="/about/" component={About}/>
              <Route path="/contacts" component={Contacts}/>
              <Route path="/posts/:id" component={FullPost}/>
              <Route path="/subscribe" component={Subscribe}/>
              <Route path="/chat" component={Chat}/>
          </Switch>
          <Footer title="Blog Team" description="See you soon." />
      </BrowserRouter>
  );
};

export default App;
