import axios from 'axios'

const axiosBlog = axios.create({
    baseURL: 'https://js9-blog-kim-default-rtdb.firebaseio.com/'
});

export default axiosBlog;